from reportlab.lib.pagesizes import letter
from PyPDF2 import PdfFileWriter, PdfFileReader, PdfFileMerger
from reportlab.pdfgen import canvas
import io
import jalal
from arabic_reshaper import ArabicReshaper
from bidi.algorithm import get_display
import persian
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont



def spbycoma(string):
    string=string[::-1]
    li = [string[i:i+3] for i in range(0, len(string), 3)]
    li = li[::-1]
    string = ','.join(li)
    return persian.convert_en_numbers(string)

def convertTojalali(data):
    return persian.convert_en_numbers(jalal.Gregorian(data).persian_string())

def makeRTL(string):
    configuration = {'language':'Farsi'}
    reshaper = ArabicReshaper(configuration=configuration)
    return get_display(reshaper.reshape(string))

def editPdf(dick):
    pdfmetrics.registerFont(TTFont('Shabnam', 'Shabnam.ttf'))
    #conifg arabic reshaper
    # meke new pdf
    packet = io.BytesIO()
    pdf =canvas.Canvas(packet)
    pdf.setFont("Shabnam", 14)
    #make new pdf with size of orginal pdf  to write text into it
    pdf.setPageSize((716 , 449))
    # draw data on pdf
    #shomare sefaresh
    pdf.drawString(614 - ( len(dick['order_id'])*7 ),416 ,dick['order_id'] )
    # zaman sodor
    pdf.drawString(124 - ( len(dick['order_date'])*7 )  ,416 ,convertTojalali( dick['order_date'] ))
    #menu
    pdf.drawString(546 - ( len(dick['menu'])*7 )  ,356 , makeRTL( dick['menu'] ))
    #mahsole aval
    pdf.drawString(630 - ( len(dick['row1']['desc1'])*7 )  ,245 , makeRTL( dick['row1']['desc1'] ) )
    #meghdare aval
    pdf.drawString(350 - ( len(dick['row1']['amount1'])*7 ),245 ,makeRTL(dick['row1']['amount1']))
    #gheimat aval
    pdf.drawString(225 - ( len(dick['row1']['price1'])*7 ),245 ,spbycoma(dick['row1']['price1']))
    #mablagh aval
    pdf.drawString(130  - ( len(dick['row1']['total1'])*7 ) ,245 ,spbycoma(dick['row1']['total1']))
    #mahsole dovom
    pdf.drawString(630 - ( len(dick['row2']['desc2'])*7 ),200 ,makeRTL(dick['row2']['desc2'] ))
    #meghdare dovom
    pdf.drawString(350  - ( len(dick['row2']['amount2'])*7 ),200 , makeRTL( dick['row2']['amount2']  )  )
    #gheimate dovom
    pdf.drawString(225  - ( len(dick['row2']['price2'])*7 ),200 ,spbycoma( dick['row2']['price2']  ))
    #gmablaghe dovom
    pdf.drawString(130 - ( len(dick['row2']['total2'])*7 ),200 ,spbycoma( dick['row2']['total2']  ))
    #mablghe kol
    pdf.drawString(310 - ( len(dick['all_price'])*7 ),146 , spbycoma( dick['all_price']  ) )
    #molahezat
    pdf.drawString(636 - ( len(dick['desc'])*7 ),91 , makeRTL( dick['desc'] ))
    pdf.save()

    # merge pdfs
    packet.seek(0)
    new_pdf = PdfFileReader(packet)
    ex_pdf = PdfFileReader(open("source.pdf","rb"))
    outpout = PdfFileWriter()
    page = ex_pdf.getPage(0)
    page.mergePage(new_pdf.getPage(0))
    outpout.addPage(page)
    outStream = open("des.pdf", "wb")
    outpout.write(outStream)
    outStream.close()


if __name__ == "__main__":
    editPdf(dick)
